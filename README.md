# gosync
Push/pull GitLab snippets from the terminal

## Usage

### List all snippets
```sh
gosync list
```

### Create/Update snippet
```sh
gosync push /path/to/file
gosync push /path/to/file name-of-snippet
```

### Pull snippet
```sh
gosync pull snippet-name
gosync pull snippet-name target-path
```
