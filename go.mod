module gitlab.com/dandrii/gosync

go 1.15

require (
	github.com/adrg/xdg v0.3.0
	gopkg.in/yaml.v2 v2.4.0
)
