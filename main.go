package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"

	"github.com/adrg/xdg"
	"gopkg.in/yaml.v2"
)

type Config struct {
	APIKey string `yaml:"apiKey"`
}

type Snippet struct {
	ID       int    `json:"id"`
	Title    string `json:"title"`
	FileName string `json:"file_name"`
	WebURL   string `json:"web_url"`
}

var config = &Config{}

var client = &http.Client{
	Transport: &http.Transport{},
}

var usage = `Usage:
> gosync list

> gosync push /path/to/file-or-dir
> gosync push /path/to/file-or-dir name-of-snippet

> gosync pull snippet-name
> gosync pull snippet-name target-path
`

func main() {
	guardArgs(2)

	if _, err := getGlobalConfig(); err != nil {
		log.Fatal(err)
	}

	switch command := os.Args[1]; command {
	case "push":
		guardArgs(3)
		push()
	case "pull":
		guardArgs(3)
		pull()
	case "list":
		list()
	default:
		fmt.Print(usage)
		os.Exit(1)
	}
}

func guardArgs(min int) {
	if len(os.Args) < min {
		fmt.Println(usage)
		os.Exit(1)
	}
}

func getGlobalConfig() (*Config, error) {
	configFile, err := xdg.ConfigFile("dandrii/gosync/config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	if _, err := os.Stat(configFile); err != nil {
		if os.IsNotExist(err) {
			file, err := os.Create(configFile)
			if err != nil {
				return nil, err
			}
			file.Close()
		} else {
			return nil, err
		}
	}

	content, err := ioutil.ReadFile(configFile)
	if err != nil {
		return nil, err
	}

	if err := yaml.Unmarshal(content, config); err != nil {
		return nil, err
	}

	if config.APIKey == "" {
		fmt.Print("Your API token\n> ")
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		config.APIKey = scanner.Text()

		configBytes, err := yaml.Marshal(config)
		if err != nil {
			log.Fatal(err)
		}

		if err := ioutil.WriteFile(configFile, configBytes, 0644); err != nil {
			log.Fatal(err)
		}
	}

	return config, nil
}

func listSnippets() []Snippet {
	req, err := http.NewRequest("GET", "https://gitlab.com/api/v4/snippets", nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Add("PRIVATE-TOKEN", config.APIKey)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)

	s := []Snippet{}
	json.Unmarshal(b, &s)

	return s
}

func updateSnippet(s *Snippet, content string) {
	f := map[string]string{"action": "update", "file_path": s.FileName, "content": content}
	file, err := json.Marshal(f)
	if err != nil {
		log.Fatal(err)
	}

	body := []byte(fmt.Sprintf(`{"files":[%s]}`, file))

	u := fmt.Sprintf("https://gitlab.com/api/v4/snippets/%d", s.ID)
	req, err := http.NewRequest("PUT", u, bytes.NewBuffer(body))
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Add("PRIVATE-TOKEN", config.APIKey)
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	r, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode > 399 {
		log.Println(string(body))
		log.Fatal(string(r))
	}
}

func createSnippet(snippetname string, filename string, content string) *Snippet {
	f := map[string]string{"file_path": filename, "content": content}
	file, err := json.Marshal(f)
	if err != nil {
		log.Fatal(err)
	}

	body := []byte(fmt.Sprintf(`{"title":"%s","visibility":"private","files":[%s]}`, snippetname, file))

	req, err := http.NewRequest("POST", "https://gitlab.com/api/v4/snippets", bytes.NewBuffer(body))
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Add("PRIVATE-TOKEN", config.APIKey)
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	r, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode > 399 {
		log.Println(string(body))
		log.Fatal(string(r))
	}

	s := &Snippet{}
	json.Unmarshal(r, s)

	return s
}

func getSnippet(id int) []byte {
	url := fmt.Sprintf("https://gitlab.com/api/v4/snippets/%d/raw", id)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Add("PRIVATE-TOKEN", config.APIKey)
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	r, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	if resp.StatusCode > 399 {
		log.Fatal(string(r))
	}

	return r
}

func find(a []Snippet, x string) *Snippet {
	for _, n := range a {
		if x == n.Title {
			return &n
		}
	}
	return nil
}

func confirm(question string) bool {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print(question)
		char, _, err := reader.ReadRune()

		if err != nil {
			log.Fatal(err)
		}

		switch char {
		case '\n', 'Y', 'y':
			return true
		case 'N', 'n':
			return false
		}
	}
}

func push() {
	p := os.Args[2]

	s, err := os.Stat(p)
	if err != nil {
		log.Fatal(err)
	}

	if s.IsDir() {
		fmt.Printf("directories are not supported")
		os.Exit(1)
	} else {
		// we read given file and convert it to string
		file, err := ioutil.ReadFile(p)
		if err != nil {
			log.Fatal(err)
		}
		content := string(file)

		var name string

		// if 4th argument is given it will be used as snippet name
		// otherwise filename will be used
		if len(os.Args) == 4 {
			name = os.Args[3]
		} else {
			name = path.Base(p)
		}

		// if snippet with given name already exists, we update it
		// otherwise we create one
		l := listSnippets()
		exist := find(l, name)
		if exist != nil {
			if ok := confirm(fmt.Sprintf("Title: %s,\nURL: %s\nCorrect (Y/n)?\n> ", exist.Title, exist.WebURL)); ok {
				updateSnippet(exist, content)
			}
		} else {
			created := createSnippet(name, path.Base(p), content)
			fmt.Printf("Title: %s,\nURL: %s\n", created.Title, created.WebURL)
		}
	}
}

func pull() {
	// the third argument is used as a snippet name
	p := os.Args[2]

	l := listSnippets()
	s := find(l, p)

	if s != nil {
		b := getSnippet(s.ID)

		var target string

		// if 4th argument is given it will be used as target pass
		// snippet name will be used otherwise
		if len(os.Args) == 4 {
			target = os.Args[3]
		} else {
			target = p
		}

		ioutil.WriteFile(target, b, 0644)
	} else {
		fmt.Printf("Snippet doesn't exist: %s\n", p)
	}
}

func list() {
	l := listSnippets()
	for _, s := range l {
		fmt.Printf("\nTitle: %s,\nURL: %s\n", s.Title, s.WebURL)
	}
}
